var PlayerButtons = {
   media: null,
   mediaTimer: null,
   isPlaying: false,
   initMedia: function(path) {
      PlayerButtons.media = new Media(
         path,
         function() {
            console.log('Media file readed succesfully');
            if (PlayerButtons.media !== null)
               PlayerButtons.media.release();
            PlayerButtons.resetLayout();
         },
         function(error) {
            navigator.notification.alert(
               'Unable to read the media file.',
               function(){},
               'Error'
            );
            PlayerButtons.changePlayButton('play');
            console.log('Unable to read the media file (Code): ' + error.code);
         }
      );
   },
   playPause: function(path) {
      $.mobile.changePage('gameButtons.html')
   },
   stop: function() {
      if (PlayerButtons.media !== null)
      {
         PlayerButtons.media.stop();
         PlayerButtons.media.release();
      }
      clearInterval(PlayerButtons.mediaTimer);
      PlayerButtons.media = null;
      PlayerButtons.isPlaying = false;
      PlayerButtons.resetLayout();
   },
   resetLayout: function() {
      $('#media-played').text(Utility.formatTime(0));
      PlayerButtons.changePlayButton('play');
      PlayerButtons.updateSliderPosition(0);
   },
   updateSliderPosition: function(seconds) {
      var $slider = $('#time-slider');

      if (seconds < $slider.attr('min'))
         $slider.val($slider.attr('min'));
      else if (seconds > $slider.attr('max'))
         $slider.val($slider.attr('max'));
      else
         $slider.val(Math.round(seconds));

      $slider.slider('refresh');
   },
   seekPosition: function(seconds) {
      if (PlayerButtons.media === null)
         return;

      PlayerButtons.media.seekTo(seconds * 1000);
      PlayerButtons.updateSliderPosition(seconds);
   },
   changePlayButton: function(imageName) {
      var background = $('#player-play')
      .css('background-image')
      .replace('url(', '')
      .replace(')', '');

      $('#player-play').css(
         'background-image',
         'url(' + background.replace(/images\/.*\.png$/, 'images/' + imageName + '.png') + ')'
      );
   }
};