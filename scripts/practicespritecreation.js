(function () {
        var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update});
        var gameTargetOne;
        var gameTargetTwo;
        var gameTargetThree;
        var gameTargetFour;
        var gameButtonOne;
        var gameButtonTwo;
        var gameButtonThree;
        var gameButtonFour;
        var doWeHaveAKick = false;
        var arrayKickCount = -1;
        var currentSpriteX = [0, 175, 425, 600];
        var currentSpriteY = [840, 800, 800, 840];
        var buttonSprites = ['redparticle', 'blueparticle', 'greenparticle', 'yellowparticle'];
        var scorePraise;
        var styleOfNumberUp = {font: "35px Arial Bold", fill: "#FFFFFF", allign: 'center'};
        var score = 0 
        var scoreNumberUp = score 
        var addNumberUp;
        var gameHasStarted = false
        var loadingAnim;
        var perfectTotal = 0
        var awesomeTotal = 0
        var goodTotal = 0
        var poorTotal = 0
        var missTotal = 0
        var groups = [];

        var dancer = new Dancer();

        Dancer.setOptions({
    flashSWF : '../../src/soundmanager2.swf',
    flashJS  : '../../src/soundmanager2.js'
  });

         if (!(localStorage.getItem('songDirectory') === null)){
        dancer.load({src: localStorage.getItem('songDirectory'), codecs:['mp3', 'ogg', 'wma']})
        musicSelectionInHouse = new Media(localStorage.getItem('songDirectory')) 
        }
        else {
            dancer.load({src: 'assets/Song.mp3', codecs:['mp3', 'ogg', 'wma']})
            musicSelectionInHouse = new Media('assets/song.mp3')
        }

        dancer.setVolume(0)
        musicSelectionInHouse.setVolume(0.0)
        var songLength = musicSelectionInHouse.getDuration
 

function preload ()  {
    this.game.load.image('loading', 'assets/pictures/loading.png') 
var loadingBar = this.add.sprite(470, 240,"loading");
    loadingBar.anchor.setTo(0.5,0.5);
    this.load.setPreloadSprite(loadingBar); 
    this.game.load.image('crosshairs1', 'assets/pictures/crosshairs.png');
    this.game.load.image('crosshairs2', 'assets/pictures/crosshairs.png');
    this.game.load.image('crosshairs3', 'assets/pictures/crosshairs.png');
    this.game.load.image('crosshairs4', 'assets/pictures/crosshairs.png');
    this.game.load.image('Button1', 'assets/pictures/Button.jpg');
    this.game.load.image('Button2', 'assets/pictures/Button.jpg');
    this.game.load.image('Button3', 'assets/pictures/Button.jpg');
    this.game.load.image('Button4', 'assets/pictures/Button.jpg');
    this.game.load.image('redparticle', 'assets/pictures/redparticle.png');
    this.game.load.image('blueparticle', 'assets/pictures/blueparticle.png');
    this.game.load.image('greenparticle', 'assets/pictures/greenparticle.png');
    this.game.load.image('yellowparticle', 'assets/pictures/yellowparticle.png');
    this.game.load.image('+1', 'assets/pictures/plus1.png');
    this.game.load.image('+2', 'assets/pictures/plus2.png');
    this.game.load.image('+3', 'assets/pictures/plus3.png');
    this.game.load.image('+5', 'assets/pictures/plus5.png');
    this.game.load.image('+10', 'assets/pictures/plus10.png');
    this.game.load.image('-1', 'assets/pictures/minus1.png');
    this.game.load.image('miss', 'assets/pictures/miss.png');
    this.game.load.image('perfect', 'assets/pictures/perfect.png');
    this.game.load.image('poor', 'assets/pictures/poor.png');
    this.game.load.image('awesome', 'assets/pictures/awesome.png');
    this.game.load.image('good', 'assets/pictures/good.png');
    this.game.load.spritesheet('bot', 'assets/pictures/running_bot.png');
    this.game.load.image('rain', 'assets/pictures/rain.png', 17, 17); 
}

function create() {
    groups[0] = this.game.add.group(); 
    groups[1] = this.game.add.group();
    groups[2] = this.game.add.group();
    groups[3] = this.game.add.group();

    this.game.maxPointers = 1;

      var emitterbackground = this.game.add.emitter(this.game.world.centerX, 0, 400);
            emitterbackground.width = game.world.width;
            emitterbackground.makeParticles ('rain');
            emitterbackground.minParticleScale = 0.1;
            emitterbackground.maxParticleScale = 0.8; 
            emitterbackground.setYSpeed(300, 500);
            emitterbackground.setXSpeed(-5, 5);
            emitterbackground.minRotation = 0;
            emitterbackground.maxRotation = 0;
            emitterbackground.start(false, 1600, 5, 0);
        
        scorePraise = this.game.make.emitter(this.game.world.centerX, 0, 10)
        scorePraise.gravity = 250

    gameButtonOne = this.game.add.button(0, 420, 'Button1', function(){
    if(gameTargetOne.body.overlapyY > 190 && gameTargetOne.overlapX > 100){
    groups[0].getBottom.destroy();
    score + 10
    perfectTotal + 1
    scorePraise.makeParticles('perfect','+10');
    scorePraise.x = 100
    scorePraise.y = 100
    scorePraise.start(true, 1000, null, 2, false);
    }
    else if(gameTargetOne.body.overlapY > 175){
        groups[0].getBottom.destroy();
        score + 3
        awesomeTotal + 1
        scorePraise.makeParticles('awesome', '+3')
        scorePraise.x = 100
        scorePraise.y = 100
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if(gameTargetOne.body.overlapY > 155){
        groups[0].getBottom.destroy();
        score + 2 
        goodTotal + 1
        scorePraise.makeParticles('good', '+2')
        scorePraise.x = 100
        scorePraise.y = 100
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetOne.body.overlapY > 95){
        groups[0].getBottom.destroy();
        score + 1
        poorTotal + 1
        scorePraise.makeParticles('poor', '+1')
        scorePraise.x = 100
        scorePraise.y = 100
        scorePraise.start(true, 1000, null, 2, false)
    }
    else {
        groups[0].getBottom.destroy();
        score - 1
        missTotal + 1
        scorePraise.makeParticles('miss', '-1')
        scorePraise.x = 100
        scorePraise.y = 100
        scorePraise.start(true, 1000, null, 2, false)
    }
}, this)
    gameButtonOne.scale.x = 0.3333
    gameButtonOne.scale.y = 0.36
    






    gameButtonTwo = this.game.add.button(0, 420, 'Button2', function(){
    if(gameTargetTwo.body.overlapyY > 190){
    groups[0].getBottom.destroy();
    score + 10
    perfectTotal + 1
    scorePraise.makeParticles('perfect','+10');
    scorePraise.x = 275
    scorePraise.y = 275
    scorePraise.start(true, 1000, null, 2, false);
    }
    else if(gameTargetTwo.body.overlapY > 175){
        groups[0].getBottom.destroy();
        score + 3
        awesomeTotal + 1
        scorePraise.makeParticles('awesome', '+3')
        scorePraise.x = 275
        scorePraise.y = 275
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if(gameTargetTwo.body.overlapY > 155){
        groups[0].getBottom.destroy();
        score + 2 
        goodTotal + 1
        scorePraise.makeParticles('good', '+2')
        scorePraise.x = 275
        scorePraise.y = 275
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetTwo.body.overlapY > 95){
        groups[0].getBottom.destroy();
        score + 1
        poorTotal + 1
        scorePraise.makeParticles('poor', '+1')
        scorePraise.x = 275
        scorePraise.y = 275
        scorePraise.start(true, 1000, null, 2, false)
    }
    else {
        groups[0].getBottom.destroy();
        score - 1
        missTotal + 1
        scorePraise.makeParticles('miss', '-1')
        scorePraise.x = 275
        scorePraise.y = 275
        scorePraise.start(true, 1000, null, 2, false)
    }
}, this)
    gameButtonTwo.scale.x = 0.3333
    gameButtonTwo.scale.y = 0.36





    gameButtonThree = this.game.add.button(0, 420, 'Button3', function(){
    if(gameTargetThree.body.overlapyY > 190){
    groups[0].getBottom.destroy();
    score + 10
    perfectTotal + 1
    scorePraise.makeParticles('perfect','+10');
    scorePraise.x = 525
    scorePraise.y = 525
    scorePraise.start(true, 1000, null, 2, false);
    }
    if(gameTargetThree.body.overlapY > 175){
        groups[0].getBottom.destroy();
        score + 3
        awesomeTotal + 1
        scorePraise.makeParticles('awesome', '+3')
        scorePraise.x = 525
        scorePraise.y = 525
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if(gameTargetThree.body.overlapY > 155){
        groups[0].getBottom.destroy();
        score + 2 
        goodTotal + 1
        scorePraise.makeParticles('good', '+2')
        scorePraise.x = 525
        scorePraise.y = 525
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetThree.body.overlapY > 95){
        groups[0].getBottom.destroy();
        score + 1
        poorTotal + 1
        scorePraise.makeParticles('poor', '+1')
        scorePraise.x = 525
        scorePraise.y = 525
        scorePraise.start(true, 1000, null, 2, false)
    }
    else {
        groups[0].getBottom.destroy();
        score - 1
        missTotal + 1
        scorePraise.makeParticles('miss', '-1')
        scorePraise.x = 525
        scorePraise.y = 525
        scorePraise.start(true, 1000, null, 2, false)
    }
}, this)
    gameButtonThree.scale.x = 0.3333
    gameButtonThree.scale.y = 0.36




    gameButtonFour = this.game.add.button(0, 420, 'Button4', function(){
    if(gameTargetFour.body.overlapyY > 190){
    groups[0].getBottom.destroy();
    score + 10
    perfectTotal + 1
    scorePraise.makeParticles('perfect','+10');
    scorePraise.x = 700
    scorePraise.y = 700
    scorePraise.start(true, 1000, null, 2, false);
    }
    else if(gameTargetFour.body.overlapY > 175){
        groups[0].getBottom.destroy();
        score + 3
        awesomeTotal + 1
        scorePraise.makeParticles('awesome', '+3')
        scorePraise.x = 700
        scorePraise.y = 700
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if(gameTargetFour.body.overlapY > 155){
        groups[0].getBottom.destroy();
        score + 2 
        goodTotal + 1
        scorePraise.makeParticles('good', '+2')
        scorePraise.x = 700
        scorePraise.y = 700
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetFour.body.overlapY > 95){
        groups[0].getBottom.destroy();
        score + 1
        poorTotal + 1
        scorePraise.makeParticles('poor', '+1')
        scorePraise.x = 700
        scorePraise.y = 700
        scorePraise.start(true, 1000, null, 2, false)
    }
    else {
        groups[0].getBottom.destroy();
        score - 1
        missTotal + 1
        scorePraise.makeParticles('miss', '-1')
        scorePraise.x = 700
        scorePraise.y = 700
        scorePraise.start(true, 1000, null, 2, false)
    }
}, this)
    gameButtonFour.scale.x = 0.3333
    gameButtonFour.scale.y = 0.36

    gameTargetOne = this.game.add.sprite(0, 0, 'crosshairs1');
    game.physics.enable(gameTargetOne, Phaser.Physics.ARCADE);
        
    gameTargetTwo = this.game.add.sprite(175, 0, 'crosshairs2');
    game.physics.enable(gameTargetTwo, Phaser.Physics.ARCADE);
            
    gameTargetThree = this.game.add.sprite(425, 0,'crosshairs3');
    game.physics.enable(gameTargetThree, Phaser.Physics.ARCADE);
        
    gameTargetFour = this.game.add.sprite(600, 0, 'crosshairs4');
    game.physics.enable(gameTargetFour, Phaser.Physics.ARCADE);
    
    loadingAnim = this.game.add.sprite(this.game.world.center.x, this.game.world.center.y, 'bot')
    loadingAnim.animations.add('spin', [0, 1, 2, 3, 4, 5, 6], 10, true)
    addNumberUp = this.game.add.text(140, 30, scoreNumberUp, styleOfNumberUp)
    addNumberUp.fixedToCamera = true; 
    musicSelectionInHouse.play()
    }
function update(){
addNumberUp.text = score
if (dancerA.isLoaded === false){
loadingAnim.animations.play('spin', 10, false, false);
}
if (gameHasStarted === false){ 
if (dancerA.isLoaded === true){
dancerA.pause();
musicSelectionInHouse.play()
if (musicSelectionInHouse.getCurrentPosition >= 1.1){
musicSelectionInHouse.seekTo(0)
musicSelectionInHouse.pause();
dancerA.play() 
dancerA.onceAt(1.4, function(){
musicSelectionInHouse.play();
musicSelectionInHouse.setVolume(1.0);
loadingAnim.animations.stop(false, false)
gameHasStarted = true
});
}
}
}
if (musicSelectionInHouse.getCurrentPosition < (dancerA.getTime - 1.5) || musicSelectionInHouse.getCurrentPosition > (dancerA.getTime - 1.3)){
    musicSelectionInHouse.seekTo(dancerA.getTime-1.4);
}
if(gameHasStarted = true){
if(doWeHaveAKick === true){
    doWeHaveAKick = false;
    arrayKickCount + 1;
    var i = Math.floor(Math.random() * (5 - 0));
    spriteNumber[arrayKickCount] = this.game.add.sprite(currentSpriteX[i], currentSpriteY[i], buttonSprites[i]);
    spriteNumber[arrayKickCount].lifespan = 2000;
    this.game.physics.enable(spriteNumber[arrayKickCount], Phaser.Physics.ARCADE);
    spriteNumber[arrayKickCount].body.velocity.y = -450;
    groups[i].add(spriteNumber[arrayKickCount], true);
}
}
    if(musicSelectionInHouse.getCurrentPosition > songLength - 1){
        setTimeout(function(){localStorage.setItem('score', score); localStorage.setItem('perfect', perfectTotal); localStorage.setItem('awesome', awesomeTotal); localStorage.setItem('good', goodTotal); localStorage.setItem('poor', poorTotal); localStorage.setItem('miss', missTotal); this.game.state.start('gameover')},1000)
    }
}
 window.dancer = dancer;
})();
