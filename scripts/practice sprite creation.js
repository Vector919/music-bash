(function () {
var gameHasStarted = false;
var leaderBoardButton;
var playButton;
var visualizerButton; 

var background;
var backgroundSpeed = 1;

var gameTitleImage;
var companyLogoImage;

var selectSong;
var onePossibleFolderPath = "c:\\music";
var listOfSongs = ['Bom bom Macklemore'];
var okButton;
var addText;
var addHeading;
var buttonAppearsBleep;

var dancerA = new Dancer();
var songSelection = "assets/music/Song";


var gameButtonOne;
var gameButtonTwo;
var gameButtonThree;
var gameButtonFour;
var gameButtonOneL;
var gameButtonTwoL;
var gameButtonThreeL;
var gameButtonFourL; 
/*var farLeftEmitter;
var leftEmitter;
var rightEmitter;
var farRightEmitter;
var particleGenerators = [farLeftEmitter, leftEmitter, rightEmitter, farRightEmitter];*/
var gameButtons = [gameButtonOneL, gameButtonTwoL, gameButtonThreeL, gameButtonFourL];
var buttonSprites = ['redparticle', 'blueparticle', 'greenparticle', 'yellowparticle'];
var currentSpriteX = [0, 175, 425, 600];
var currentSpriteY = [-240, -200, -200, -240];
var currentSprite;
var lastSpriteColumn = []

var timing;
var music;
var score = 0;
var spriteNumber = [];
var np = -1;
var arrayKickCount = -1;
var totalPossibleScore = arrayKickCount + 1;
var leftMostSprite;
var leftSprite;
var rightSprite;
var rightMostSprite;
var positionYAtPress = [];
var positionXAtPress = [];

var bot;
var isDancerPaused = false
var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create});

 Dancer.setOptions({
    flashSWF : '../../sound libraries/soundmanager2.swf',
    flashJS  : '../../sound libraries/soundmanager2.js'
  });

function displayKickCount (){
        console.log(arrayKickCount)}

function preload() {

    //  You can fill the preloader with as many assets as your game requires

    //  Here we are loading an image. The first parameter is the unique
    //  string by which we'll identify the image later in our code.

    //  The second parameter is the URL of the image (relative)
  
    game.load.image('okbutton', 'assets/pictures/okbutton.png');
    game.load.image('companylogo', 'assets/pictures/logo.png');
    game.load.image('gametitle', 'assets/pictures/gametitlemedium.png');
    game.load.image('background', 'assets/pictures/background.jpg');
    game.load.image('play', 'assets/pictures/play.png'); 
    game.load.image('visualizer', 'assets/pictures/visualizer.png');
    game.load.image('leaderboard', 'assets/pictures/leaderboard.png');
    game.load.image('song select menu', 'assets/pictures/songselect.png');
    game.load.image('crosshairs1', 'assets/pictures/crosshairs.png');
    game.load.image('crosshairs2', 'assets/pictures/crosshairs.png');
    game.load.image('crosshairs3', 'assets/pictures/crosshairs.png');
    game.load.image('crosshairs4', 'assets/pictures/crosshairs.png');
    game.load.image('redparticle', 'assets/pictures/redparticle.png');
    game.load.image('blueparticle', 'assets/pictures/blueparticle.png');
    game.load.image('greenparticle', 'assets/pictures/greenparticle.png');
    game.load.image('yellowparticle', 'assets/pictures/yellowparticle.png');
    game.load.audio('buttonappearsbleep', 'assets/sfx/buttonappearsbleep.mp3');
    game.load.image('bot', 'assets/pictures/running_bot.png');
    game.load.spritesheet('rain', 'assets/pictures/rain.png', 17, 17);
//this is not the actual method of importing music into the app. Just for testing purposes.
    game.load.audio('Bom bom Macklemore', 'assets/music/BombomMacklemore.mp3');
    music = game.add.audio('Bom bom Macklemore');
    dancerA.load({src: songSelection, codecs:['mp3']})
}

kick = dancerA.createKick({
onKick: function() {  
    arrayKickCount + 1;
    var i = Math.floor(Math.random() * (5 - 0));
    lastSpriteColumn[arrayKickCount] = i
    if (lastSpriteColumn[arrayKickCount - 1] === lastSpriteColumn[arrayKickCount]) {
        i + 1;
        if (i > 4) 
            {i - 2;}
    }
    spriteNumber[arrayKickCount] = game.add.sprite(currentSpriteX[i], currentSpriteY[i], buttonSprites[i]);
    spriteNumber[arrayKickCount].lifespan = 1500
    spriteNumber[arrayKickCount].inputEnabled = true;
    game.physics.enable(spriteNumber[arrayKickCount], Phaser.Physics.ARCADE);
    spriteNumber[arrayKickCount].body.velocity.y = 450
if (i === 0 || i === 600){
    spriteNumber[arrayKickCount].events.onInputDown.add(checkTimingOfPress);
}
else {
    spriteNumber[arrayKickCount].events.onInputDown.add(checkTimingOfPressTwo);
}
}
}).on();

function create() {


buttonAppearsBleep = game.add.audio('buttonappearsbleep');
    buttonAppearsBleep.addMarker('buttonappearsbleep', 0, 0.1);
    buttonAppearsBleep.volume = .01;
    var textHeading = "READ ME!";
    var styleOfHeading = {font: "45px Arial", fill: "#DF0101", allign: 'center'};
    addHeading = game.add.text(game.world.centerX-130, 10, textHeading, styleOfHeading);
    var text = " This awesome rhythm game lets you play with your own music!\n Pretty amazing huh? I know! However, there is a catch. In\n order for the app to use your music it needs to be able to find your\n music.\n So what does this mean for you? Unfortunately, if you download\n your music via your device through Google Play you're going to have\n a tough time accessing those folders. Google Play stores music in a\n secure hidden folder.\n So what's to be done? Thankfully, the solution is simple. Store your\n songs on your sd card or in your general directory.\n My suggestion: regardless of where you download your music use\n Windows Media Player or a similiar third party software to sync\n your device.";   
    var style = {font: "25px Arial", fill: "#FFFFFF", align: 'left'};
    addText = game.add.text(game.world.centerX-374, 80, text, style);
    setTimeout(function addOkayButton() {
    okButton = game.add.sprite(250, 440, 'okbutton');
    okButton.inputEnabled = true;
    okButton.events.onInputDown.add(createDisclaimer, this);
    buttonAppearsBleep.play('buttonappearsbleep');
    }, 2500);
}
    function createDisclaimer() {
    game.world.remove(addText);
    game.world.remove(addHeading);
    okButton.exists = false;
    companyLogoImage = game.add.sprite(game.world.centerX, game.world.centerY, 'companylogo');
    companyLogoImage.anchor.set(.5);
    companyLogoImage.inputEnabled = true;
    setTimeout(function removeLogo(){companyLogoImage.exists = false; createOpeningMenu()}, 6000);
}
function createOpeningMenu(){   
    setTimeout(function createBackground(){background = game.add.sprite(180, 0, 'background');}, 1000);
    setTimeout(function presentTitle() {gameTitleImage = game.add.sprite(60, 30,'gametitle');}, 1000);
    setTimeout(function addButtons() {
        leaderBoardButton = game.add.sprite(400, 465, 'leaderboard'); 
        leaderBoardButton.inputEnabled = true
        leaderBoardButton.events.onInputDown.add(actionOnClick, actionOnClickPlus, this);}, 1000);
    setTimeout(function addMoreButtons(){
        visualizerButton = game.add.sprite(145, 350, 'visualizer');
        visualizerButton.inputEnabled = true
        visualizerButton.events.onInputDown.add(actionOnClick, this);}, 1000); 
    setTimeout(function addEvenMoreButtons(){
        playButton = game.add.sprite(40, 210, 'play');
        playButton.inputEnabled = true
        playButton.events.onInputDown.add(testTheGame, this);}, 1000);
}

function actionOnClick () {
    leaderBoardButton.exists = false;
    playButton.exists = false;
    visualizerButton.exists = false;
    gameTitleImage.visible = false; 
    } 

function actionOnClickPlus() {
   
}

function testTheGame(){
    companyLogoImage.exists = false;
    leaderBoardButton.exists = false;
    playButton.exists = false;
    visualizerButton.exists = false;
    gameTitleImage.visible = false;
    background.visible = false;
    setTheStage();
    makeButtonsAndEmitters();
    dancerA.play();
    dancerA.setVolume(0);
    setTimeout(function giveMeMusic(){
    music.play();
    }, 2000);
}


    function setTheStage(){ 
    var emitter = game.add.emitter(game.world.centerX, 0, 400);
            emitter.width = game.world.width;
            emitter.makeParticles ('rain');
            emitter.minParticleScale = 0.1;
            emitter.maxParticleScale = 0.8; 
            emitter.setYSpeed(300, 500);
            emitter.setXSpeed(-5, 5);
            emitter.minRotation = 0;
            emitter.maxRotation = 0;
            emitter.start(false, 1600, 5, 0);
        }
function checkTimingOfPressFarLeft(){}
function checkTimingOfPressLeft(){}
function checkTimingOfPressRight(){}
function checkTimingOfPressFarRight(){}

function makeButtonsAndEmitters(){ 
    gameButtonOne = game.add.sprite(0, 380, 'crosshairs1');
    gameButtonOne.inputEnabled = true;
    gameButtonOne.events.onInputOut.add(checkTimingOfPressFarLeft, this);
    gameButtonOneL = gameButtonOne.x
    
    gameButtonTwo = game.add.sprite(175, 420, 'crosshairs2');
    gameButtonTwo.inputEnabled = true; 
    gameButtonTwo.events.onInputOut.add(checkTimingOfPressLeft, this);
    gameButtonTwoL = gameButtonTwo.x
        
    gameButtonThree = game.add.sprite(425, 420,'crosshairs3');
    gameButtonThree.inputEnabled = true;
    gameButtonThree.events.onInputOut.add(checkTimingOfPressRight, this);
    gameButtonThreeL = gameButtonThree.x
    
    gameButtonFour = game.add.sprite(600, 380, 'crosshairs4');
    gameButtonFour.inputEnabled = true;
    gameButtonFour.events.onInputOut.add(checkTimingOfPressFarRight, this);
   } 

function checkTimingOfPress(){
    if (spriteNumber[arrayKickCount].x < 385 && spriteNumber[arrayKickCount].x > 380) {
        score + 10; //perfect
        score - 1;
        spriteNumber[arrayKickCount].kill;
    }
    else if (spriteNumber[arrayKickCount].x < 395 && spriteNumber[arrayKickCount].x > 365) {
        score + 3; //awesome
        score - 1;
        spriteNumber[arrayKickCount].kill()  
    }
    else if (spriteNumber[arrayKickCount].x < 405 && spriteNumber[arrayKickCount].x > 360) {
        score + 2; //good
        score - 1;
        spriteNumber[arrayKickCount].kill();
        }
    else if (spriteNumber[arrayKickCount].x < 415 && spriteNumber[arrayKickCount].x > 350) {
    score + 1;
    score - 1;
    spriteNumber[arrayKickCount].kill();
    }//poor
    else {score + 0;} //miss
} 

function checkTimingOfPressTwo(){
    if (spriteNumber[arrayKickCount].y < 425 && spriteNumber[arrayKickCount].y > 420) {
        score + 10; //perfect
        score - 1;
        spriteNumber[arrayKickCount].kill();  
        }
    else if (spriteNumber[arrayKickCount].y < 435 && spriteNumber[arrayKickCount].y > 405) {
        score + 3; //awesome
        score - 1; 
        spriteNumber[arrayKickCount].kill(); 
        }
    else if (spriteNumber[arrayKickCount].y < 445 && spriteNumber[arrayKickCount].y > 400) {
        score + 2; //good
        score - 1;
        spriteNumber[arrayKickCount].kill();
        }
    else if (spriteNumber[arrayKickCount].y < 455 && spriteNumber[arrayKickCount].y > 390) {
        score + 1;
        score - 1;
        spriteNumber[arrayKickCount].kill();
        } //poor
    else {score - 2;} //miss    
    }

  window.dancerA = dancerA;
})();