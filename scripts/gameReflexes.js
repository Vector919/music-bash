	(function () {
    var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'reflexes', {preload: preload, create: create, update: update});
        var doWeHaveAKick = false;
        var arrayKickCount = -1;
        var currentSpriteX = [0, 175, 425, 600];
        var currentSpriteY = [-240, -200, -200, -240];
        var buttonSprites = ['redparticle', 'blueparticle', 'greenparticle', 'yellowparticle'];
        var scorePraise;
        var styleOfNumberUp = {font: "35px Arial Bold", fill: "#FFFFFF", allign: 'center'};
        var score = 0 
        var scoreNumberUp = score 
        var addNumberUp;
        var gameHasStarted = false
        var loadingAnim;
        var perfectTotal = 0
        var awesomeTotal = 0
        var goodTotal = 0
        var poorTotal = 0
        var missTotal = 0

        var dancer = new Dancer();

        Dancer.setOptions({
    flashSWF : '../../src/soundmanager2.swf',
    flashJS  : '../../src/soundmanager2.js'
  });

        if (!(localStorage.getItem('songDirectory') === null)){
        dancerA.load({src: localStorage.getItem('songDirectory'), codecs:['mp3', 'ogg', 'wma']})
        musicSelectionInHouse = new Media(localStorage.getItem('songDirectory')) 
        }
        else {
            dancerA.load({src: 'assets/Song.mp3', codecs:['mp3', 'ogg', 'wma']})
            musicSelectionInHouse = new Media('assets/song.mp3')
        }

        dancerA.setVolume(0)
        musicSelectionInHouse.setVolume(0.0)
        var songLength = musicSelectionInHouse.getDuration 

        function preload ()  {
    this.game.load.image('loading', 'assets/pictures/loading.png') 
var loadingBar = this.add.sprite(470, 240,"loading");
    loadingBar.anchor.setTo(0.5,0.5);
    this.load.setPreloadSprite(loadingBar); 
    this.game.load.image('redparticle', 'assets/pictures/redparticle.png');
    this.game.load.image('blueparticle', 'assets/pictures/blueparticle.png');
    this.game.load.image('greenparticle', 'assets/pictures/greenparticle.png');
    this.game.load.image('yellowparticle', 'assets/pictures/yellowparticle.png');
    this.game.load.image('+1', 'assets/pictures/plus1.png');
    this.game.load.image('+2', 'assets/pictures/plus2.png');
    this.game.load.image('+3', 'assets/pictures/plus3.png');
    this.game.load.image('+5', 'assets/pictures/plus5.png');
    this.game.load.image('+10', 'assets/pictures/plus10.png');
    this.game.load.image('-1', 'assets/pictures/minus1.png');
    this.game.load.image('miss', 'assets/pictures/miss.png');
    this.game.load.image('perfect', 'assets/pictures/perfect.png');
    this.game.load.image('poor', 'assets/pictures/poor.png');
    this.game.load.image('awesome', 'assets/pictures/awesome.png');
    this.game.load.image('good', 'assets/pictures/good.png');
    this.game.load.spritesheet('bot', 'assets/pictures/running_bot.png');
    this.game.load.image('rain', 'assets/pictures/rain.png', 17, 17); 
}
kick = dancerA.createKick({
onKick: function() {  
doWeHaveAKick = true 
}
}).on();


gameTitle.prototype = {
      create: function(){
    this.game.maxPointers = 1;

      var emitterbackground = this.game.add.emitter(this.game.world.centerX, 0, 400);
            emitterbackground.width = game.world.width;
            emitterbackground.makeParticles ('rain');
            emitterbackground.minParticleScale = 0.1;
            emitterbackground.maxParticleScale = 0.8; 
            emitterbackground.setYSpeed(300, 500);
            emitterbackground.setXSpeed(-5, 5);
            emitterbackground.minRotation = 0;
            emitterbackground.maxRotation = 0;
            emitterbackground.start(false, 1600, 5, 0);
        
        scorePraise = this.game.make.emitter(this.game.world.centerX, 0, 10)
        scorePraise.gravity = 250


    loadingAnim = this.game.add.sprite(this.game.world.center.x, this.game.world.center.y, 'bot')
    loadingAnim.animations.add('spin', [0, 1, 2, 3, 4, 5, 6], 10, true)
    addNumberUp = this.game.add.text(140, 30, scoreNumberUp, styleOfNumberUp)
    addNumberUp.fixedToCamera = true; 
    musicSelectionInHouse.play()
    },

update: function(){
addNumberUp.text = score
if (dancerA.isLoaded === false){
loadingAnim.animations.play('spin', 10, false, false);
}
if (gameHasStarted === false){ 
if (dancerA.isLoaded === true){
dancerA.pause();
musicSelectionInHouse.play()
if (musicSelectionInHouse.getCurrentPosition >= 1.1){
musicSelectionInHouse.seekTo(0)
musicSelectionInHouse.pause();
dancerA.play() 
dancerA.onceAt(0.5, function(){
musicSelectionInHouse.play();
musicSelectionInHouse.setVolume(1.0);
loadingAnim.animations.stop(false, false)
gameHasStarted = true
})
}
}
}
if (musicSelectionInHouse.getCurrentPosition < (dancerA.getTime - 0.6) || musicSelectionInHouse.getCurrentPosition > (dancerA.getTime - 0.4)){
    musicSelectionInHouse.seekTo(dancerA.getTime-0.5);
}
 if(musicSelectionInHouse.getCurrentPosition > songLength - 1){
        setTimeout(function(){localStorage.setItem('score', score); localStorage.setItem('perfect', perfectTotal); localStorage.setItem('awesome', awesomeTotal); localStorage.setItem('good', goodTotal); localStorage.setItem('poor', poorTotal); localStorage.setItem('miss', missTotal); this.game.state.start('gameover')}, 1000)
}
if(gameHasStarted = true){
if(doWeHaveAKick === true){
    doWeHaveAKick = false;
    arrayKickCount + 1;
    var i = Math.floor(Math.random() * (5 - 0));
    var randomLocationX = Math.floor(Math.random()*(625-0))
    var randomLocationY = Math.floor(Math.random()*(425-0))
    spriteNumber[arrayKickCount] = this.game.add.sprite(randomLocationX, randomLocationY, buttonSprites[i]);
    spriteNumber[arrayKickCount].scale.set(0) 
    var motion = this.game.add.tween(spriteNumber[arrayKickCount].scale)
    motion.to({x:1, y: 1}, 500, Phaser.Easing.Linear, false)
    motion.chain(this.game.add.tween(spriteNumber[arrayKickCount].scale).to({x: 0, y: 0}, 500, Phaser.Easing.Linear, 200, false));
    spriteNumber[arrayKickCount].lifespan = 1000
    this.game.physics.enable(spriteNumber[arrayKickCount], Phaser.Physics.ARCADE);
    spriteNumber[arrayKickCount].inputEnabled = true; 
    spriteNumber[arrayKickCount].events.onInputDown.add(function(){
        if (this.game.input.activePointer.targetObject.scale.x >= .95 && this.game.input.activePointer.targetObject.scale.y >= .95){
            this.game.input.activePointer.targetObject.destroy();
            score + 10; //perfect
            perfectTotal + 1
            scorePraise.makeParticles('perfect','+10');
            scorePraise.x = this.game.input.activePointer.x;
            scorePraise.y = this.game.input.activePointer.y;
            scorePraise.start (true, 1000, null, 2, false);
}
         else if (this.game.input.activePointer.targetObject.scale.x > .85 && thisgame.input.activePointer.targetObject.scale.y > .85) {
            this.game.input.activePointer.targetObject.destroy();
            score + 3;
            awesomeTotal + 1
            scorePraise.makeParticles('awesome', '+3')
            scorePraise.x = this.game.input.activePointer.x 
            scorePraise.y = this.game.input.activePointer.y
            scorePraise.start(true, 1000, null, 2, false)

         }
        else if (this.game.input.activePointer.targetObject.scale.x > .60 && this.game.input.activePointer.targetObject.scale.y > .60){
            this.game.input.activePointer.targetObject.destroy(); 
            score + 2
            goodTotal + 1
            scorePraise.makeParticles('good', '+2')
            scorePraise.x = this.game.input.activePointer.x 
            scorePraise.y = this.game.input.activePointer.y
            scorePraise.start(true, 1000, null, 2, false)
         }
      else if (this.game.input.activePointer.targetObject.scale.x > .30 && this.game.input.activePointer.targetObject.scale.y > .30){
            this.game.input.activePointer.targetObject.destroy(); 
            score + 1
            poorTotal + 1
            scorePraise.makeParticles('poor', '+1')
            scorePraise.x = this.game.input.activePointer.x 
            scorePraise.y = this.game.input.activePointer.y
            scorePraise.start(true, 1000, null, 2, false)   
}
   else {
        this.game.input.activePointer.targetObject.destroy();
        score - 1 
        missTotal + 1
        scorePraise.makeParticles('miss', '-1')
        scorePraise.x = this.game.input.activePointer.x
        scorePraise.y = this.game.input.activePointer.y
        scorePraise.start(true, 1000, null, 2, false)
        }
}, this);
}
}
}
}
   