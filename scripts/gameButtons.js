(function () {
    dWidth = $(window).width();
    dHeight = $(window).height();

        var gameTargetOne;
        var gameTargetTwo;
        var gameTargetThree;
        var gameTargetFour;
        var gameButtonOne;
        var gameButtonTwo;
        var gameButtonThree;
        var gameButtonFour;
        var doWeHaveAKick = false;
        var arrayKickCount = -1;
        var currentSpriteX = [0, 175, 425, 600];
        var currentSpriteY = [840, 800, 800, 840];
        var buttonSprites = ['redparticle', 'blueparticle', 'greenparticle', 'yellowparticle'];
        var scorePraise;
        var styleOfNumberUp = {font: "35px Arial Bold", fill: "#FFFFFF", allign: 'center'};
        var score = 0 
        var scoreNumberUp = score 
        var addNumberUp;
        var gameHasStarted = false
        var loadingAnim;
        var perfectTotal = 0
        var awesomeTotal = 0
        var goodTotal = 0
        var poorTotal = 0
        var missTotal = 0
        var groups = [];
        var addNumberUp;
        var musicSelectionInHouse;
        var loadingBar;
        var songDirectory;
        if (!(window.sessionStorage.getItem('SongDirectory') == null)){
        songDirectory = window.sessionStorage.getItem('SongDirectory')
        var pos = songDirectory.lastIndexOf('.')
        var songDirectoryDancer = songDirectory.slice(0, pos)
    }

        var dancerA = new Dancer();

        Dancer.setOptions({
    flashSWF : '../../build/sound libraries/soundmanager2.swf',
    flashJS  : '../../build/sound libraries/soundmanager2.js'
  });

       /*if (!(songDirectory == null)){
        dancerA.load({src: songDirectoryDancer, codecs:['mp3', 'ogg', 'wma']})
        musicSelectionInHouse = new Media(songDirectory) 
         }*/
       // else {
            dancerA.load({src: 'assets/music/Song', codecs:['mp3']})
           // }

    if (dWidth/dHeight > 8/5 - .1 && dWidth/dHeight < 8/5 + .1) {
    var game = new Phaser.Game(800, 1280, Phaser.CANVAS, 'buttons', { preload: preload, create: create});
    }
    else if (dWidth/dHeight > 16/9 - .1 && dWidth/dHeight < 16/9 + .1){
    var game = new Phaser.Game(800, 1424, Phaser.CANVAS, 'buttons', { preload: preload, create: create});   
    }
    else if(dWidth/dHeight > 4/3 - .3 && dWidth/dHeight < 4/3 + .3){
    var game = new Phaser.Game(800, 1068, Phaser.CANVAS, 'buttons', { preload: preload, create: create});   
    }
    else if(dWidth/dHeight > 3/2 - .15 && dWidth/dHeight < 3/2 - .15){
    var game = new Phaser.Game(800, 1200, Phaser.CANVAS, 'buttons', { preload: preload, create: create});   
    }
    else{var game = new Phaser.Game(800, 1200, Phaser.CANVAS, 'buttons', { preload: preload, create: create});} 

function preload ()  {
    game.load.image('crosshairs1', 'assets/pictures/crosshairs.png');
    game.load.image('crosshairs2', 'assets/pictures/crosshairs.png');
    game.load.image('crosshairs3', 'assets/pictures/crosshairs.png');
    game.load.image('crosshairs4', 'assets/pictures/crosshairs.png');
    game.load.image('Button1', 'assets/pictures/Button.jpg');
    game.load.image('Button2', 'assets/pictures/Button.jpg');
    game.load.image('Button3', 'assets/pictures/Button.jpg');
    game.load.image('Button4', 'assets/pictures/Button.jpg');
    game.load.image('redparticle', 'assets/pictures/redparticle.png');
    game.load.image('blueparticle', 'assets/pictures/blueparticle.png');
    game.load.image('greenparticle', 'assets/pictures/greenparticle.png');
    game.load.image('yellowparticle', 'assets/pictures/yellowparticle.png');
    game.load.image('+1', 'assets/pictures/plus1.png');
    game.load.image('+2', 'assets/pictures/plus2.png');
    game.load.image('+3', 'assets/pictures/plus3.png');
    game.load.image('+5', 'assets/pictures/plus5.png');
    game.load.image('+10', 'assets/pictures/plus10.png');
    game.load.image('-1', 'assets/pictures/minus1.png');
    game.load.image('miss', 'assets/pictures/miss.png');
    game.load.image('perfect', 'assets/pictures/perfect.png');
    game.load.image('poor', 'assets/pictures/poor.png');
    game.load.image('awesome', 'assets/pictures/awesome.png');
    game.load.image('good', 'assets/pictures/good.png');
    game.load.spritesheet('bot', 'assets/pictures/running_bot.png', 102, 102);
    game.load.image('rain', 'assets/pictures/rain.png', 17, 17); 
      
}
kick = dancerA.createKick({
onKick: function() {  
doWeHaveAKick = true 
}
}).on();


function create() {
    dancerA.play();
    game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
    game.scale.setScreenSize();

    gameTargetOne = game.add.sprite(0, 0, 'crosshairs1');
    game.physics.enable(gameTargetOne, Phaser.Physics.ARCADE);
        
    gameTargetTwo = game.add.sprite(175, 0, 'crosshairs2');
    game.physics.enable(gameTargetTwo, Phaser.Physics.ARCADE);
            
    gameTargetThree = game.add.sprite(425, 0,'crosshairs3');
    game.physics.enable(gameTargetThree, Phaser.Physics.ARCADE);
        
    gameTargetFour = game.add.sprite(600, 0, 'crosshairs4');
    game.physics.enable(gameTargetFour, Phaser.Physics.ARCADE);
    
    loadingAnim = game.add.sprite(game.world.centerX, game.world.centerY, 'bot')
    loadingAnim.animations.add('spin', [0, 1, 2, 3, 4, 5, 6], 8, true)
    loadingAnim.animations.play('spin', 8, true, false);
    addNumberUp = game.add.text(140, 30, scoreNumberUp, styleOfNumberUp)
    addNumberUp.fixedToCamera = true; 
    //musicSelectionInHouse.play()

    groups[0] = game.add.group(); 
    groups[1] = game.add.group();
    groups[2] = game.add.group();
    groups[3] = game.add.group();

    game.maxPointers = 4;

      var emitterbackground = game.add.emitter(game.world.centerX, 0, 400);
            emitterbackground.width = game.world.width;
            emitterbackground.makeParticles ('rain');
            emitterbackground.minParticleScale = 0.1;
            emitterbackground.maxParticleScale = 0.8; 
            emitterbackground.setYSpeed(300, 500);
            emitterbackground.setXSpeed(-5, 5);
            emitterbackground.minRotation = 0;
            emitterbackground.maxRotation = 0;
            emitterbackground.start(false, 3000, 5, 0);
        
        scorePraise = game.make.emitter(this.game.world.centerX, 0, 10)
        scorePraise.gravity = 250

    gameButtonOne = game.add.button(0, 1060, 'Button1', function(){
    if(gameTargetOne.body.overlapyY > 190 && gameTargetOne.overlapX > 100){
    groups[0].getBottom.destroy();
    score + 10
    perfectTotal + 1
    scorePraise.makeParticles('perfect','+10');
    scorePraise.x = 100
    scorePraise.y = 100
    scorePraise.start(true, 1000, null, 2, false);
    }
    else if(gameTargetOne.body.overlapY > 175){
        groups[0].getBottom.destroy();
        score + 3
        awesomeTotal + 1
        scorePraise.makeParticles('awesome', '+3')
        scorePraise.x = 100
        scorePraise.y = 100
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if(gameTargetOne.body.overlapY > 155){
        groups[0].getBottom.destroy();
        score + 2 
        goodTotal + 1
        scorePraise.makeParticles('good', '+2')
        scorePraise.x = 100
        scorePraise.y = 100
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetOne.body.overlapY > 95){
        groups[0].getBottom.destroy();
        score + 1
        poorTotal + 1
        scorePraise.makeParticles('poor', '+1')
        scorePraise.x = 100
        scorePraise.y = 100
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetOne.body.overlapY <= 95){
        groups[0].getBottom.destroy();
        score - 1
        missTotal + 1
        scorePraise.makeParticles('miss', '-1')
        scorePraise.x = 100
        scorePraise.y = 100
        scorePraise.start(true, 1000, null, 2, false)
    }
}, this)
    gameButtonOne.scale.x = 0.3333
    gameButtonOne.scale.y = 0.36

    gameButtonTwo = game.add.button(200, 1060, 'Button2', function(){
    if(gameTargetTwo.body.overlapyY > 190){
    groups[0].getBottom.destroy();
    score + 10
    perfectTotal + 1
    scorePraise.makeParticles('perfect','+10');
    scorePraise.x = 275
    scorePraise.y = 275
    scorePraise.start(true, 1000, null, 2, false);
    }
    else if(gameTargetTwo.body.overlapY > 175){
        groups[0].getBottom.destroy();
        score + 3
        awesomeTotal + 1
        scorePraise.makeParticles('awesome', '+3')
        scorePraise.x = 275
        scorePraise.y = 275
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if(gameTargetTwo.body.overlapY > 155){
        groups[0].getBottom.destroy();
        score + 2 
        goodTotal + 1
        scorePraise.makeParticles('good', '+2')
        scorePraise.x = 275
        scorePraise.y = 275
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetTwo.body.overlapY > 95){
        groups[0].getBottom.destroy();
        score + 1
        poorTotal + 1
        scorePraise.makeParticles('poor', '+1')
        scorePraise.x = 275
        scorePraise.y = 275
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetTwo.body.overlapY <= 95){
        groups[0].getBottom.destroy();
        score - 1
        missTotal + 1
        scorePraise.makeParticles('miss', '-1')
        scorePraise.x = 275
        scorePraise.y = 275
        scorePraise.start(true, 1000, null, 2, false)
    }
}, this)
    gameButtonTwo.scale.x = 0.3333
    gameButtonTwo.scale.y = 0.36

    gameButtonThree = game.add.button(400, 1060, 'Button3', function(){
    if(gameTargetThree.body.overlapyY > 190){
    groups[0].getBottom.destroy();
    score + 10
    perfectTotal + 1
    scorePraise.makeParticles('perfect','+10');
    scorePraise.x = 525
    scorePraise.y = 525
    scorePraise.start(true, 1000, null, 2, false);
    }
    if(gameTargetThree.body.overlapY > 175){
        groups[0].getBottom.destroy();
        score + 3
        awesomeTotal + 1
        scorePraise.makeParticles('awesome', '+3')
        scorePraise.x = 525
        scorePraise.y = 525
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if(gameTargetThree.body.overlapY > 155){
        groups[0].getBottom.destroy();
        score + 2 
        goodTotal + 1
        scorePraise.makeParticles('good', '+2')
        scorePraise.x = 525
        scorePraise.y = 525
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetThree.body.overlapY > 95){
        groups[0].getBottom.destroy();
        score + 1
        poorTotal + 1
        scorePraise.makeParticles('poor', '+1')
        scorePraise.x = 525
        scorePraise.y = 525
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetThree.body.overlapY <= 95){
        groups[0].getBottom.destroy();
        score - 1
        missTotal + 1
        scorePraise.makeParticles('miss', '-1')
        scorePraise.x = 525
        scorePraise.y = 525
        scorePraise.start(true, 1000, null, 2, false)
    }
}, this)
    gameButtonThree.scale.x = 0.3333
    gameButtonThree.scale.y = 0.36


    gameButtonFour = game.add.button(600, 1060, 'Button4', function(){
    if(gameTargetFour.body.overlapyY > 190){
    groups[0].getBottom.destroy();
    score + 10
    perfectTotal + 1
    scorePraise.makeParticles('perfect','+10');
    scorePraise.x = 700
    scorePraise.y = 700
    scorePraise.start(true, 1000, null, 2, false);
    }
    else if(gameTargetFour.body.overlapY > 175){
        groups[0].getBottom.destroy();
        score + 3
        awesomeTotal + 1
        scorePraise.makeParticles('awesome', '+3')
        scorePraise.x = 700
        scorePraise.y = 700
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if(gameTargetFour.body.overlapY > 155){
        groups[0].getBottom.destroy();
        score + 2 
        goodTotal + 1
        scorePraise.makeParticles('good', '+2')
        scorePraise.x = 700
        scorePraise.y = 700
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetFour.body.overlapY > 95){
        groups[0].getBottom.destroy();
        score + 1
        poorTotal + 1
        scorePraise.makeParticles('poor', '+1')
        scorePraise.x = 700
        scorePraise.y = 700
        scorePraise.start(true, 1000, null, 2, false)
    }
    else if (gameTargetFour.body.overlapY <= 95){
        groups[0].getBottom.destroy();
        score - 1
        missTotal + 1
        scorePraise.makeParticles('miss', '-1')
        scorePraise.x = 700
        scorePraise.y = 700
        scorePraise.start(true, 1000, null, 2, false)
    }
}, this)
    gameButtonFour.scale.x = 0.3333
    gameButtonFour.scale.y = 0.36
    }
function update(){
addNumberUp.text = score
songLength = dancerA.getDuration
if (dancerA.isLoaded === null){
loadingAnim.animations.play('spin', 10, false, false);
}
if (gameHasStarted === false){ 
if (!(dancerA.isLoaded === null)){
dancerA.pause();
musicSelectionInHouse.play()
if (musicSelectionInHouse.getCurrentPosition >= 1.1){
musicSelectionInHouse.seekTo(0)
musicSelectionInHouse.pause();
dancerA.play() 
gameHasStarted = true
dancerA.onceAt(1.4, function(){
musicSelectionInHouse.play();
musicSelectionInHouse.setVolume(1.0);
loadingAnim.animations.stop(false, false)
});
}
}
}
if (musicSelectionInHouse.getCurrentPosition < (dancerA.getTime - 1.5) || musicSelectionInHouse.getCurrentPosition > (dancerA.getTime - 1.3)){
   musicSelectionInHouse.seekTo(dancerA.getTime-1.4);
}
if(gameHasStarted = true){
if(doWeHaveAKick === true){
    doWeHaveAKick = false;
    arrayKickCount + 1;
    var i = Math.floor(Math.random() * (5 - 0));
    spriteNumber[arrayKickCount] = game.add.sprite(currentSpriteX[i], currentSpriteY[i], buttonSprites[i]);
    spriteNumber[arrayKickCount].lifespan = 2000;
    game.physics.enable(spriteNumber[arrayKickCount], Phaser.Physics.ARCADE);
    spriteNumber[arrayKickCount].body.velocity.y = -450;
    groups[i].add(spriteNumber[arrayKickCount], true);
}
}
    if(musicSelectionInHouse.getDuration == dancerA.getTime){
      setTimeout(function(){window.sessionStorage.setItem('score', score); window.sessionStorage.setItem('perfect', perfectTotal); window.sessionStorage.setItem('awesome', awesomeTotal); window.sessionStorage.setItem('good', goodTotal); localStorage.setItem('poor', poorTotal); localStorage.setItem('miss', missTotal);},1000)
    }
}*/
 window.dancerA = dancerA;
})();
